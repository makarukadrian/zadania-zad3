# -*- encoding: utf-8 -*-

import getpass
import requests
from bs4 import BeautifulSoup

url = {
    'login': 'https://canvas.instructure.com/login',
    'grades': 'https://canvas.instructure.com/courses/838324/grades'
}

#username = raw_input('Użytkownik: ')
#password = getpass.getpass('Hasło: ')

payload = {
    'pseudonym_session[unique_id]': "makarukadrian@gmail.com",
    'pseudonym_session[password]': "zaq12wsx"
}

# Logowanie
session = requests.session()
session.post(url['login'], data = payload)
# Pobranie strony z ocenami
response = session.get(url['grades'])
# Odczyt strony
html = response.text
# Parsowanie strony
parsed = BeautifulSoup(html, 'html5lib')
# Scraping
aktywnosc = []
laboratoria = []
pracadom = []


lista = parsed.find(id='grades_summary').find_all('tr')
for tr in lista:

    category = tr.find_all("div", { "class" : "context" })
    for a in category:
        if a.text == u"Aktywność":
           aktywnosc.append(('%s %s %s' % (tr.a.text, tr.a['href'], tr.find('span', 'score').text)))
        if a.text == "Prace domowe":
            pracadom.append(('%s %s %s' % (tr.a.text, tr.a['href'], tr.find('span', 'score').text)))
        if a.text == "Laboratoria":
            laboratoria.append(('%s %s %s' % (tr.a.text, tr.a['href'], tr.find('span', 'score').text)))


# Sortowanie

sorted(aktywnosc, key=lambda a: a[2])
sorted(laboratoria, key=lambda a: a[2])
sorted(pracadom, key=lambda a: a[2])

# Wyświetlenie posortowanych ocen w kategoriach

for akt in aktywnosc:
    print akt
for lab in laboratoria:
    print lab
for pd in pracadom:
    print pd


