

import requests
from bs4 import BeautifulSoup
import json
import html5lib
import urllib2
from metwit import Metwit

def pogoda(item1, item2):
    try:
        weather = Metwit.weather.get(location_lat=item1, location_lng=item2)
        suma =""
        suma = "\n\nTimestamp " + weather[0]['timestamp'] + "\n"
        suma += "Status " + weather[0]['weather']['status'] + "\n"
        suma += "Country " + weather[0]['location']['country'] + "\n"
        suma += "Locality " + weather[0]['location']['locality'] + "\n"
        wiatr = weather[0]['weather']['measured']['wind_speed']
        #print wiatr
        suma += "Wind Speed %s \n" % (wiatr)
        suma += "Temperature "
        t = weather[0]['weather']['measured']['temperature']
        #print weather

        wynik = "%s: %s " %(suma, t)
        return wynik
    except:
        print 'Wystapil blad, sprawdz czy czy podana szerokosc geograficzna wskazuje na miasto'
        exit(1)


def tlumaczenie(src, dest, text, email, password, outformat):

    payload = {
        'src' : src,
        'dest' : dest,
        'text' : text,
        'email' : email,
        'password' : password,
        'outformat' : outformat
    }

    response = requests.get('http://syslang.com', params = payload)
    result = response.json()
    #print(result)
    print(result["translation"])


if __name__ == '__main__':
    print("Wspolrzedne geograficzne polskich miast mozna znalezc na stronie: \n http://cybermoon.pl/wiedza/wspolrzedne_polskich_miast.html")

    lat = raw_input("Podaj szerokosc geograficzna(np. 52.880)")
    lng = raw_input("Podaj szerokosc geograficzna(np. 18.700)")

    tmp = pogoda(lat,lng)
    tlumaczenie('en','pl',tmp,'dwampwi@wp.pl','lubiepwi123','json')


